﻿// Merging sequentially has shown to be faster, I might be doing something wrong
// Paralel merging might still end up being quicker on systems with LOTS of cores (8+)
//#define PARALELMERGING

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Threading;

namespace NET1.cviko2 {
    public class ParaSorter<T> where T:IComparable<T> {
        private T[] data;
        private T[] temp;
        private int nThreads;
        private class ThreadData {
            T[] data;
            public int beginIx, endIx; //beginIx is included in the range, endIx is excluded
#if PARALELMERGING
            public Thread[] threads;
            public ThreadData[] tDatas;
            int id;
            public ThreadData(T[] data, int beginIx, int endIx, Thread[] threads, int id, ThreadData[] tDatas) {
                this.threads = threads;
                this.id = id;
                this.tDatas = tDatas;
#else
            public ThreadData(T[] data, int beginIx, int endIx) {
#endif
                this.data = data;
                this.beginIx = beginIx;
                this.endIx = endIx;
            }
            public void SortThread() {
                Array.Sort(data, beginIx, endIx - beginIx);
#if PARALELMERGING
                //PARALLEL MERGING
                int k = 1; int nThreads = threads.Length;
                while (id + k < nThreads && id % (k << 1) == 0) {
                    threads[id + k].Join();
                    int beg = beginIx;
                    int mid = tDatas[id + k].beginIx;
                    int end = tDatas[id + k].endIx;
                    /*if (id + (k << 1) == nThreads - 1) {
                        end = data.Length;
                    }*/
                    //end2 = a == nThreads-2 ? data.Length : end2;
                    Merge(beg, mid, end);
                    tDatas[id].endIx = tDatas[id + k].endIx;
                    k <<= 1;
                }
            }
            //FOR PARALEL MERGING
            void Merge(int begin, int middle, int end) {
                List<T> tmpList = new List<T>(end - begin);
                int start = begin;
                int begin2 = middle;
                while (begin < middle && begin2 < end) {
                    if (data[begin].CompareTo(data[begin2]) < 0) {
                        tmpList.Add( data[begin++] );
                    } else {
                        tmpList.Add( data[begin2++] );
                    }
                }
                while (begin < middle) {
                    tmpList.Add( data[begin++] );
                }
                while (begin2 < end) {
                    tmpList.Add( data[begin2++] );
                }
                int ptr = 0;
                for (int i = start; i < end; ++i) {
                    data[i] = tmpList[ptr++];
                }
#endif
            }
        }
        Thread[] threads;
        ThreadData[] tDatas;
        public ParaSorter(T[] data) {
            this.data = data;
        }
        public T[] Sort(int nThreads) {
            if (data.Length < 2 * nThreads) {
                nThreads = 1;
            }
            this.nThreads = nThreads;
            threads = new Thread[nThreads];
            tDatas = new ThreadData[nThreads];
            //sort threads
            threads[0] = Thread.CurrentThread;
            
            Func<int, int> WorkChunk = (int x) => {
                return x * data.Length / nThreads;
            };
            for (int i = nThreads-1; i > 0; --i) {
#if PARALELMERGING
                tDatas[i] = new ThreadData(data, WorkChunk(i), WorkChunk(i+1), threads, i, tDatas);
                        //i == nThreads-1 ? data.Length : (i+1) * workChunk, threads, i, workChunk);
                threads[i] = new Thread(tDatas[i].SortThread);
                threads[i].Start();
            }
            tDatas[0] = new ThreadData(data, 0, WorkChunk(1), threads, 0, tDatas);
            tDatas[0].SortThread();

            for (int i = nThreads - 1; i > 0; --i)
                threads[i].Join();

            return data;
#else 
                tDatas[i] = new ThreadData(data, WorkChunk(i), WorkChunk(i + 1));
                threads[i] = new Thread(tDatas[i].SortThread);
                threads[i].Start();
            }
            tDatas[0] = new ThreadData(data, 0, WorkChunk(1));
            tDatas[0].SortThread();

            for (int i = nThreads - 1; i > 0; --i)
                threads[i].Join();

            //SEQUENTIAL_MERGING
            int a = 0, k = 1;
            temp = new T[data.Length];
            while (a + k < nThreads) {
                Merge(tDatas[a].beginIx, tDatas[a].endIx, tDatas[a+k].endIx, temp);
                tDatas[a].endIx = tDatas[a+k].endIx;
                a += k << 1;
                if (a + k >= nThreads) {
                    int mergedTill = tDatas[a - k].endIx;
                    Array.Copy(data, mergedTill, temp, mergedTill, data.Length - mergedTill);
                    a = 0; k <<= 1;
                    T[] swap = data; data = temp; temp = swap;
                }
            }
            return data;
#endif
        }

        //FOR SEQUENTIAL MERGING
        void Merge(int begin, int middle, int end, T[] target) {
            //dont tamper with middle & end
            int start = begin;
            int begin2 = middle;
            while (begin < middle && begin2 < end) {
                if (data[begin].CompareTo(data[begin2]) < 0) {
                    target[start++] = data[begin++];
                } else {
                    target[start++] = data[begin2++];
                }
            }
            while (begin < middle) {
                target[start++] = data[begin++];
            }
            while (begin2 < end) {
                target[start++] = data[begin2++];
            }
        }
        public void Print(TextWriter output) {
            foreach (T elem in data) {
                output.WriteLine(elem);
            }
        }
    }
    public class ProgramBody {
        public static void Execute(string[] args, TextReader input, TextWriter output) {
            try {
                #region input-reading
                int n;
                if (args.Length != 1 || !int.TryParse(args[0], out n)) {
                    throw new ArgumentException("Invalid number of arguments.");
                }
                if (n < 1 || n > 256) {
                    throw new ArgumentException("Invalid number of threads.");
                }
                string s = input.ReadLine();
                var list = new List<int>();
                while (s != "") {
                    int a;
                    if (s == null || !int.TryParse(s, out a)) {
                        throw new FormatException();
                    }
                    list.Add(a);
                    s = input.ReadLine();
                }
                #endregion
                var sw = new System.Diagnostics.Stopwatch();
                sw.Start();
                var sorter = new ParaSorter<int>(list.ToArray());
                sorter.Sort(n);
                sw.Stop();
                Console.WriteLine(sw.ElapsedMilliseconds + "ms");
                sorter.Print(output);
            } catch (ArgumentException) {
                output.WriteLine("Argument Error");
            } catch (FormatException) {
                output.WriteLine("Format Error");
            }
        }
    }
    
    class Program {
        static void Main(string[] args) {
            ProgramBody.Execute(args, Console.In, Console.Out);
        }
    }
}
